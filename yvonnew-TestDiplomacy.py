#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

	# -----
	# solve
	# -----
	
	def test_solve1(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC London\n")

	def test_solve2(self):
		r = StringIO("A Madrid Hold\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A Madrid\n")

	def test_solve3(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\n")

	def test_solve_4(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

	def test_solve_5(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
			
	def test_solve_6(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

	def test_solve_7(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
	
	def test_solve_8(self):
		r = StringIO("C London Move Madrid\nB Barcelona Move Madrid\nA Madrid Hold\nD Paris Move London\nE Austin Support A\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD London\nE Austin\n")

	def test_solve_9(self):
		r = StringIO("A Madrid Move London\nB London Support A\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\n")

	def test_solve_10(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")
	def test_solve_11(self):
		r = StringIO("A Madrid Hold\nB Paris Hold\nC Moscow Move Madrid\nD Kiev Support C\nE Berlin Move Paris\nF Austin Support E\nG Houston Support F\nH Dallas Support F\nI Copenhagen Move Austin\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC Madrid\nD Kiev\nE [dead]\nF Austin\nG Houston\nH Dallas\nI [dead]\n")
	



# ----


# ----
# main
# ----

if __name__ == "__main__":
	main()

