from unittest import main, TestCase
from io import StringIO

from Diplomacy import diplomacy_solve


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_solve_1(self):
        r = StringIO("A Austin Hold\nB Paris Support A\nC Houston Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB [dead]\nC [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Austin Move Dallas\nB ElPaso Move Dallas\nC SanAntonio Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Dallas\nB [dead]\nC SanAntonio\n")

    def test_solve_3(self):
        r = StringIO("A London Hold\nB Paris Hold \nC NewYork Move Houston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Paris\nC Houston\n")

    def test_solve_4(self):
        r = StringIO("A London Hold\nB Paris Move London \nC NewYork Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC NewYork\n")

    def test_solve_5(self):
        r = StringIO("A London Hold\nB Paris Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Paris\n")


if __name__ == "__main__":
    main()
