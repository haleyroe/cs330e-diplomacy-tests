# TestDiplomacy.py #

from io import StringIO
from unittest import main, TestCase

from Diplomacy import Board, Army, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


# for initalizing unit test boards #
def create_board(board, input_v):
    for line in input_v:
        board.create_army(line)


class TestDiplomacy(TestCase):

    # test file #
    def test_file(self):
        assert(True)

    # test init army #
    def test_init_army(self):
        test_army = Army('A', 'Madrid', 'Hold')
        test_list = []
        test_list.append(test_army.Name)
        test_list.append(test_army.Origin)
        test_list.append(test_army.Action)
        test_list.append(test_army.Target)
        test_list.append(test_army.Support)
        test_list.append(test_army.Status)
        self.assertEqual(test_list, ['A', 'Madrid', 'Hold', None, 0, True])

    def test_init_board_1(self):
        Board1 = Board()
        self.assertEqual(Board1.ArmyList, [])
        self.assertEqual(Board1.ArmyLocation, {})

    def test_create_army_1(self):
        Board3 = Board()
        create_board(Board3, ['A Austin Hold', 'B Houston Hold',
                              'C Dallas Hold', 'D SanAntonio Hold', 'E ElPaso Hold'])
        self.assertEqual(len(Board3.ArmyList), 5)
        #self.assertEqual(Board3.ArmyLocation, {'Austin':[], 'Houston':[], 'Dallas':[], 'SanAntonio':[], 'ElPaso':[]})

    def test_create_army_2(self):
        Board2 = Board()
        create_board(Board2, ['A Austin Hold'])
        self.assertEqual(len(Board2.ArmyList), 1)
        #self.assertEqual(Board2.ArmyLocation, {'Austin':[]})
    
    # 3 Unit Tests for diplomacy_solve #
    def test_diplomacy_solve_01(self):
        r = StringIO("A Austin Move Houston\nB Houston Hold\nC Dallas Support A\nD SanAntonio Support A\nE ElPaso Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Houston\nB [dead]\nC Dallas\nD SanAntonio\nE ElPaso\n")
    
    def test_diplomacy_solve_02(self):
        r = StringIO("A Austin Move Dallas\nB Houston Move Austin\nC Dallas Move Houston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Dallas\nB Austin\nC Houston\n")
            
    def test_diplomacy_solve_03(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Support Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")    

    # test diplomacy_solve: Given Cases #
    def test_diplomacy_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_diplomacy_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_diplomacy_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_diplomacy_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_solve_5(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_diplomacy_solve_6(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_diplomacy_solve_7(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_diplomacy_solve_8(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()
