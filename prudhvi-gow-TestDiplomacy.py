#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_read, diplomacy_solve


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----
    def test_read_1(self):
        s = StringIO("A Paris Hold\nB Dallas Move Paris\n")
        string_list = diplomacy_read(s)

        self.assertEqual(string_list[0], ["A", "Paris", "Hold"])
        self.assertEqual(string_list[1], ["B", "Dallas", "Move", "Paris"])

    def test_read_2(self):
        s = StringIO("A London Hold\nB Seattle Move Toronto\nC Toronto Hold\n")
        string_list = diplomacy_read(s)

        self.assertEqual(string_list[0], ["A", "London", "Hold"])
        self.assertEqual(string_list[1], ["B", "Seattle", "Move", "Toronto"])
        self.assertEqual(string_list[2], ["C", "Toronto", "Hold"])

    def test_read_3(self):
        s = StringIO("")
        string_list = diplomacy_read(s)
        self.assertEqual(string_list, [])

    def test_read_4(self):
        s = StringIO("\n")
        string_list = diplomacy_read(s)

        self.assertEqual(string_list[0], [])
    # ----
    # eval
    # # ----

    def test_eval_1(self):
        r = diplomacy_eval([["A", "Paris", "Hold"], ["B" ,"Dallas", "Move", "Paris"]])
        self.assertEqual(r, [('A', '[dead]'), ('B', '[dead]')])

    def test_eval_2(self):
        r = diplomacy_eval(
            [["A", "Austin", "Hold"], ["B", "Dallas", "Support", "A"], ["C", "NewYork", "Move", "Austin"]])
        self.assertEqual(r, [("A", "Austin"), ("B", "Dallas"), ("C", "[dead]")])
    
    def test_eval_3(self):
        r = diplomacy_eval(
            [["A", "London", "Support", "B"], ["B", "Dallas", "Hold"], ["C", "Seattle", "Support", "B"], ["D", "Austin", "Move", "Dallas"]])
        self.assertEqual(r, [("A", "London"), ("B", "Dallas"), ("C", "Seattle"), ("D", "[dead]")])

    def test_eval_4(self):
        r = diplomacy_eval(
            [["A", "London", "Hold"], ["B", "Toronto", "Support", "A"], ["C", "Jakarta", "Move", "Toronto"], ["D", "LasVegas", "Support", "C"]])
        self.assertEqual(r, [("A", "London"), ("B", "[dead]"), ("C", "Toronto"), ("D", "LasVegas")])

    def test_eval_5(self):
        r = diplomacy_eval(
            [["A", "Madrid", "Move", "Austin"], ["B", "Austin", "Hold"], ["C", "Dallas", "Support", "B"]])
        self.assertEqual(r, [("A", "[dead]"), ("B", "Austin"), ("C", "Dallas")])

    def test_eval_6(self):
        r = diplomacy_eval(
            [["A", "London", "Hold"], ["B", "Toronto", "Support", "A"], ["C", "Jakarta", "Move", "London"], ["D", "LasVegas", "Support", "C"], ["E", "Portland", "Support", "C"]])
        self.assertEqual(r, [("A", "[dead]"), ("B", "Toronto"), ("C", "London"), ("D", "LasVegas"), ("E", "Portland")])
    
    def test_eval_7(self):
        r = diplomacy_eval(
            [["A", "London", "Hold"], ["B", "Toronto", "Support", "A"], ["C", "Jakarta", "Move", "Toronto"], ["D", "LasVegas", "Move", "London"], ["E", "Portland", "Support", "D"]])
        self.assertEqual(r, [("A", "[dead]"), ("B", "[dead]"), ("C", "[dead]"), ("D", "London"), ("E", "Portland")])
    # ----
    # print
    # ----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [("A", "[dead]"), ("B", "[dead]")])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [("A", "Madrid"), ("B", "NewDelhi"),
                            ("C", "Athens"), ("D", "Baltimore"), ("E", "Portland")])
        self.assertEqual(
            w.getvalue(), "A Madrid\nB NewDelhi\nC Athens\nD Baltimore\nE Portland\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(
            w, [("A", "[dead]"), ("B", "SanAntonio"), ("C", "Miami"), ("D", "[dead]")])
        self.assertEqual(
            w.getvalue(), "A [dead]\nB SanAntonio\nC Miami\nD [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


if __name__ == "__main__": #pragma: no cover
    main()
